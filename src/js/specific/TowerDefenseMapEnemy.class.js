/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


// TODO rm magic values
var TOWER_DEFENSE_MAP_ENEMY_DEFAULT_WIDTH  = 50;
var TOWER_DEFENSE_MAP_ENEMY_DEFAULT_HEIGHT = 50;
var TOWER_DEFENSE_MAP_ENEMY_DEFAULT_POSITION_X = - TOWER_DEFENSE_MAP_ENEMY_DEFAULT_WIDTH;
var TOWER_DEFENSE_MAP_ENEMY_DEFAULT_POSITION_Y = 300 - TOWER_DEFENSE_MAP_ENEMY_DEFAULT_HEIGHT / 2;
var TOWER_DEFENSE_MAP_ENEMY_DEFAULT_LIFE_MAX = 1;
var TOWER_DEFENSE_MAP_ENEMY_DEFAULT_DISTANCE_PER_MICROSECONDS = 0.1;


/**
 * @constructor
 */
function TowerDefenseMapEnemy(x, y)
{
	"use strict";
	
	this.rectangle = new Rectangle(
		new Position2D(x, y),
		TOWER_DEFENSE_MAP_ENEMY_DEFAULT_WIDTH,
		TOWER_DEFENSE_MAP_ENEMY_DEFAULT_HEIGHT
	);
	
	// TODO rm magic value
	this.life = TOWER_DEFENSE_MAP_ENEMY_DEFAULT_LIFE_MAX;
}

TowerDefenseMapEnemy.createDefault = function()
{
	"use strict";
	return new TowerDefenseMapEnemy(
		TOWER_DEFENSE_MAP_ENEMY_DEFAULT_POSITION_X,
		TOWER_DEFENSE_MAP_ENEMY_DEFAULT_POSITION_Y
	);
};


TowerDefenseMapEnemy.prototype.getPosition = function()
{
	"use strict";
	return this.rectangle.position;
};

TowerDefenseMapEnemy.prototype.getWidth = function()
{
	"use strict";
	return this.rectangle.width;
};

TowerDefenseMapEnemy.prototype.getHeight = function()
{
	"use strict";
	return this.rectangle.height;
};

TowerDefenseMapEnemy.prototype.reduceLife = function(value)
{
	"use strict";
	if(this.life >= value)
	{
		this.life = 0;
	}
	else
	{
		this.life -= value;
	}
};

TowerDefenseMapEnemy.prototype.isDestroyed = function()
{
	"use strict";
	return this.life == 0;
};

TowerDefenseMapEnemy.prototype.destroy = function()
{
	"use strict";
	this.life = 0;
};

TowerDefenseMapEnemy.prototype.update = function(clock)
{
	"use strict";
	this.rectangle.position.x += clock.deltaTime * TOWER_DEFENSE_MAP_ENEMY_DEFAULT_DISTANCE_PER_MICROSECONDS;
};
