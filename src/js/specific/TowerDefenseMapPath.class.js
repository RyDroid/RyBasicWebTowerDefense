/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 */
function TowerDefenseMapPath()
{
	"use strict";
	
	this.position = new Position2D(0, 0);
	// TODO rm magic values
	this.position.x =   0;
	this.width      = 800;
	this.position.y = 250;
	this.height     = 100;
	
	this.enemies = [];
}

TowerDefenseMapPath.prototype.getPosition = function()
{
	"use strict";
	return this.position;
};

TowerDefenseMapPath.prototype.getWidth = function()
{
	"use strict";
	return this.width;
};

TowerDefenseMapPath.prototype.getHeight = function()
{
	"use strict";
	return this.height;
};

TowerDefenseMapPath.prototype.addEnemy = function(enemy)
{
	"use strict";
	if(!(enemy instanceof TowerDefenseMapEnemy))
	{
		throw new TypeError('enemy is not a TowerDefenseMapEnemy');
	}
	this.enemies.push(enemy);
};

TowerDefenseMapPath.prototype.addDefaultEnemyAtBeginning = function()
{
	"use strict";
	var enemy = TowerDefenseMapEnemy.createDefault();
	this.addEnemy(enemy);
};

TowerDefenseMapPath.prototype.updateEnemies = function(clock)
{
	"use strict";
	for(var i=0; i < this.enemies.length; ++i)
	{
		this.enemies[i].update(clock);
	}
};

TowerDefenseMapPath.prototype.doCollisionsWithAttacks = function(attacks)
{
	var nb_destroyed_enemies = 0;
	var nb_destroyed_attacks = 0;
	var nb_enemies = this.enemies.length;
	var nb_attacks = attacks.length;
	
	for(var i=0; i < nb_attacks; ++i)
	{
		var attack = attacks[i];
		for(var j=0; j < nb_enemies; ++j)
		{
			var enemy = this.enemies[j];
			if(enemy.rectangle.isInCollisionWith(attack.rectangle))
			{
				attack.destroy();
				++nb_destroyed_attacks;
				--nb_attacks;
				attacks[i] = attacks[nb_attacks];
				
				enemy.reduceLife(attack.power);
				if(enemy.isDestroyed())
				{
					++nb_destroyed_enemies;
					--nb_enemies;
					this.enemies[this.enemies.length - nb_destroyed_enemies] = enemy;
					break;
				}
			}
		}
	}
	
	this.enemies.splice(-1, nb_destroyed_enemies);
	attacks.splice(-1, nb_destroyed_attacks);
};

TowerDefenseMapPath.prototype.update = function(clock, attacks)
{
	"use strict";
	this.updateEnemies(clock);
	this.doCollisionsWithAttacks(attacks);
};
