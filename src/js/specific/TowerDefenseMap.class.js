/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 */
function TowerDefenseMap()
{
	"use strict";
	
	this.path = new TowerDefenseMapPath();
	this.attackers = [];
	this.attacks   = [];
}

TowerDefenseMap.prototype.addAttacker = function(attacker)
{
	if(!(attacker instanceof TowerDefenseMapAttacker))
	{
		throw new TypeError('attacker is not a TowerDefenseMapAttacker');
	}
	
	// TODO collision path
	
	for(var i=0; i < this.attackers.length; ++i)
	{
		// TODO collision
		if(this.attackers[i].getPosition().equals(attacker.getPosition()))
		{
			return false;
		}
	}
	
	this.attackers.push(attacker);
	return true;
};

TowerDefenseMap.prototype.updateAttackers = function(clock)
{
	for(var i=0; i < this.attackers.length; ++i)
	{
		var attacker = this.attackers[i];
		attacker.update(clock);
	}
};

TowerDefenseMap.prototype.updateAttacks = function(clock)
{
	for(var i=0; i < this.attacks.length; ++i)
	{
		var attack = this.attacks[i];
		attack.update(clock);
	}
};

TowerDefenseMap.prototype.removeUselessAttacks = function()
{
	var nb_destroyed = 0;
	var nb_attacks = this.attacks.length;
	
	for(var i=0; i < nb_attacks;)
	{
		var attack = this.attacks[i];
		// TODO rm magic values
		if(attack.getPosition().x > 800 || attack.getPosition().y > 600)
		{
			++nb_destroyed;
			--nb_attacks;
			this.attacks[i] = this.attacks[nb_attacks];
		}
		else
		{
			++i;
		}
	}
	
	this.attacks.splice(-1, nb_destroyed);
};

TowerDefenseMap.prototype.update = function(clock)
{
	this.path.update(clock, this.attacks);
	this.updateAttackers(clock);
	this.updateAttacks(clock);
};
