/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


// TODO rm magic values
var TOWER_DEFENSE_MAP_ATTACKER_DEFAULT_WIDTH  = 50;
var TOWER_DEFENSE_MAP_ATTACKER_DEFAULT_HEIGHT = 50;
var TOWER_DEFENSE_MAP_ATTACKER_DEFAULT_TIME_MS_TO_PRODUCE = 500;


/**
 * @constructor
 */
function TowerDefenseMapAttacker(x, y, attacks)
{
	"use strict";
	
	this.rectangle = new Rectangle(
		new Position2D(x, y),
		TOWER_DEFENSE_MAP_ATTACKER_DEFAULT_WIDTH,
		TOWER_DEFENSE_MAP_ATTACKER_DEFAULT_HEIGHT
	);
	
	this.attacks = attacks;
	this.timeMsToProduce = TOWER_DEFENSE_MAP_ATTACKER_DEFAULT_TIME_MS_TO_PRODUCE;
	this.timeMsBeforeNextProduction = this.timeMsToProduce;
};

TowerDefenseMapAttacker.prototype.getPosition = function()
{
	"use strict";
	return this.rectangle.position;
};

TowerDefenseMapAttacker.prototype.getWidth = function()
{
	"use strict";
	return this.rectangle.width;
};

TowerDefenseMapAttacker.prototype.getHeight = function()
{
	"use strict";
	return this.rectangle.height;
};

TowerDefenseMapAttacker.prototype.produceForce = function(clock)
{
	"use strict";
	this.timeMsBeforeNextProduction = this.timeMsToProduce;
	if(Array.isArray(this.attacks))
	{
		var x = this.rectangle.position.x + this.rectangle.width  / 2 - TOWER_DEFENSE_MAP_ATTACK_DEFAULT_WIDTH  / 2;
		var y = this.rectangle.position.y + this.rectangle.height / 2 - TOWER_DEFENSE_MAP_ATTACK_DEFAULT_HEIGHT / 2;
		this.attacks.push(new TowerDefenseMapAttack(
			x, y,
			0,
			y < 300 ? 1 : -1 // TODO rm magic values
		));
	}
};

TowerDefenseMapAttacker.prototype.update = function(clock)
{
	"use strict";
	if(clock.deltaTime >= this.timeMsBeforeNextProduction)
	{
		this.produceForce();
	}
	else
	{
		this.timeMsBeforeNextProduction -= clock.deltaTime;
	}
};
