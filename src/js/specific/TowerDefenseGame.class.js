/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 */
function TowerDefenseGame()
{
	"use strict";
	
	this.canvas = document.querySelector("canvas");
	// TODO rm magic values
	this.canvas.width  = 800;
	this.canvas.height = 600;
	this.context = this.canvas.getContext("2d");
	
	this.map = new TowerDefenseMap();
	
	this.clock = new Clock();
	this.timeInMillisecondsBetweenEachFrame = 40;
	this.frameDeltaTimeFactor = 1;
	this.idDrawingForAFrame = null;
}

TowerDefenseGame.prototype.setWindowSizeOnly = function()
{
	"use strict";
	// TODO
	//this.canvas.height = window.innerHeight;
	//this.canvas.width = this.canvas.height * 600/800;
};

TowerDefenseGame.prototype.setElementsDrawingInformation = function()
{
	"use strict";
	// TODO
};

TowerDefenseGame.prototype.setWindowSize = function()
{
	"use strict";
	this.setWindowSizeOnly();
	this.setElementsDrawingInformation();
};

TowerDefenseGame.prototype.getPercentageReady = function()
{
	"use strict";
	// TODO
	return 1;
};

TowerDefenseGame.prototype.isReady = function()
{
	"use strict";
	// TODO
	return true;
};

TowerDefenseGame.prototype.isOver = function()
{
	"use strict";
	// TODO
	return false;
};

TowerDefenseGame.prototype.addEventListeners = function()
{
	"use strict";
	// TODO
};

TowerDefenseGame.prototype.removeEventListeners = function()
{
	"use strict";
	// TODO
};

TowerDefenseGame.prototype.requestAnimationFrameCallback = function()
{
	this.updateAndDraw();
	
	// Without this condition, even after cancelAnimationFrame, this function is called!
	if(this.idDrawingForAFrame != null)
	{
		this.idDrawingForAFrame = window.requestAnimationFrame(this.requestAnimationFrameCallback.bind(this));
	}
};

TowerDefenseGame.prototype.start = function()
{
	this.setElementsDrawingInformation();
	
	function waitReady(element)
	{
		setTimeout(function() {
			/* This verification fix problem with not fully loaded images (NS_ERROR_NOT_AVAILABLE) */
			if(element.isReady())
			{
				element.addEventListeners();
				element.clock.start();
				if(typeof(window.requestAnimationFrame) == 'function' && typeof(window.cancelAnimationFrame) == 'function')
				{
					element.idDrawingForAFrame = 0;
					element.idDrawingForAFrame = window.requestAnimationFrame(element.requestAnimationFrameCallback.bind(element));
				}
				else
				{
					element.idDrawingForAFrame = window.setInterval(element.updateAndDraw.bind(element), element.timeInMillisecondsBetweenEachFrame);
				}
			}
			else
			{
				waitReady(element);
			}
		}, 10, element);
	}
	waitReady(this);
};

TowerDefenseGame.prototype.stop = function()
{
	// TODO
};

TowerDefenseGame.prototype.lose = function()
{
	// TODO
};

TowerDefenseGame.prototype.update = function()
{
	"use strict";
	
	if(this.isOver())
	{
		this.lose();
	}
	else
	{
		this.clock.update();
		this.frameDeltaTimeFactor = this.clock.getDeltaTime() / this.timeInMillisecondsBetweenEachFrame;
		// If the computer has enough power to make a frame each this.timeInMillisecondsBetweenEachFrame, factor=1
		// But, if for example the computer needs more time to make a frame, this factor will permit to run the game as the normal speed even if there is less FPS
		
		this.map.update(this.clock);
	}
};

TowerDefenseGame.prototype.updateAndDraw = function()
{
	"use strict";
	this.update();
	TowerDefenseGameDrawer2D.drawGame(this);
};
