/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var TowerDefenseGameDrawer2D = {};

TowerDefenseGameDrawer2D.drawBackground = function(canvas, context)
{
	"use strict";
	ObjectDrawer2D.drawBackgroundWithStyle(canvas, context, "rgb(20, 20, 20)");
};

TowerDefenseGameDrawer2D.drawMapEnemy = function(context, enemy)
{
	"use strict";
	context.fillStyle = "rgba(255, 0, 0, 100)";
	context.fillRect(enemy.getPosition().x, enemy.getPosition().y, enemy.getWidth(), enemy.getHeight());
};

TowerDefenseGameDrawer2D.drawMapEnemies = function(context, enemies)
{
	"use strict";
	
	if(isNaN(enemies.length))
	{
		throw new TypeError('enemies has no length');
	}
	
	for(var i=0; i < enemies.length; ++i)
	{
		TowerDefenseGameDrawer2D.drawMapEnemy(context, enemies[i]);
	}
};

TowerDefenseGameDrawer2D.drawMapPathWithoutContent = function(context, path)
{
	"use strict";
	context.fillStyle = "rgb(200, 200, 200)";
	context.fillRect(
		path.getPosition().x, path.getPosition().y,
		path.width, path.height
	);
};

TowerDefenseGameDrawer2D.drawMapPath = function(context, path)
{
	"use strict";
	TowerDefenseGameDrawer2D.drawMapPathWithoutContent(context, path);
	TowerDefenseGameDrawer2D.drawMapEnemies(context, path.enemies);
};

TowerDefenseGameDrawer2D.drawMapAttacker = function(context, attacker)
{
	"use strict";
	context.fillStyle = "rgba(0, 255, 0, 100)";
	context.fillRect(
		attacker.getPosition().x, attacker.getPosition().y,
		attacker.getWidth(), attacker.getHeight()
	);
};

TowerDefenseGameDrawer2D.drawMapAttackers = function(context, attackers)
{
	"use strict";
	
	if(isNaN(attackers.length))
	{
		throw new TypeError('attackers has no length');
	}
	
	for(var i=0; i < attackers.length; ++i)
	{
		TowerDefenseGameDrawer2D.drawMapAttacker(context, attackers[i]);
	}
};

TowerDefenseGameDrawer2D.drawMapAttack = function(context, attack)
{
	"use strict";
	context.fillStyle = "rgba(0, 0, 255, 100)";
	context.fillRect(
		attack.getPosition().x, attack.getPosition().y,
		attack.getWidth(), attack.getHeight()
	);
};

TowerDefenseGameDrawer2D.drawMapAttacks = function(context, attacks)
{
	"use strict";
	
	if(isNaN(attacks.length))
	{
		throw new TypeError('attacks has no length');
	}
	
	for(var i=0; i < attacks.length; ++i)
	{
		TowerDefenseGameDrawer2D.drawMapAttack(context, attacks[i]);
	}
};

TowerDefenseGameDrawer2D.drawMap = function(context, map)
{
	"use strict";
	TowerDefenseGameDrawer2D.drawMapPath     (context, map.path);
	TowerDefenseGameDrawer2D.drawMapAttackers(context, map.attackers);
	TowerDefenseGameDrawer2D.drawMapAttacks  (context, map.attacks);
};

TowerDefenseGameDrawer2D.drawGame = function(game)
{
	"use strict";
	TowerDefenseGameDrawer2D.drawBackground(game.canvas, game.context);
	TowerDefenseGameDrawer2D.drawMap(game.context, game.map);
};
