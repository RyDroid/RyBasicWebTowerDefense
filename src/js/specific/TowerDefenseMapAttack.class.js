/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


// TODO rm magic values
var TOWER_DEFENSE_MAP_ATTACK_DEFAULT_WIDTH  = 20;
var TOWER_DEFENSE_MAP_ATTACK_DEFAULT_HEIGHT = 20;
var TOWER_DEFENSE_MAP_ATTACK_DEFAULT_DISTANCE_PER_MICROSECONDS = TOWER_DEFENSE_MAP_ENEMY_DEFAULT_DISTANCE_PER_MICROSECONDS * 2;
var TOWER_DEFENSE_MAP_ATTACK_DEFAULT_LIFE_MAX = 1;
var TOWER_DEFENSE_MAP_ATTACK_DEFAULT_POWER    = 1;


/**
 * @constructor
 */
function TowerDefenseMapAttack(x, y, directionFactorX, directionFactorY, distancePerMs, lifeMax, power)
{
	"use strict";
	
	this.rectangle = new Rectangle(
		new Position2D(x, y),
		TOWER_DEFENSE_MAP_ATTACK_DEFAULT_WIDTH,
		TOWER_DEFENSE_MAP_ATTACK_DEFAULT_HEIGHT
	);
	
	this.setDirectionFactorX(directionFactorX);
	this.setDirectionFactorY(directionFactorY);
	
	if(distancePerMs == undefined)
	{
		this.distancePerMs = TOWER_DEFENSE_MAP_ATTACK_DEFAULT_DISTANCE_PER_MICROSECONDS;
	}
	else
	{
		this.setDistancePerMs(distancePerMs);
	}
	
	this.lifeMax     = TOWER_DEFENSE_MAP_ATTACK_DEFAULT_LIFE_MAX;
	this.lifeCurrent = this.lifeMax;
	
	if(power == undefined)
	{
		this.power = TOWER_DEFENSE_MAP_ATTACK_DEFAULT_POWER;
	}
	else
	{
		this.setPower(power);
	}
};


TowerDefenseMapAttack.prototype.isDestroyed = function()
{
	"use strict";
	return this.lifeCurrent == 0;
};

TowerDefenseMapAttack.prototype.destroy = function()
{
	"use strict";
	this.lifeCurrent = 0;
};

TowerDefenseMapAttack.prototype.reduceLife = function(value)
{
	"use strict";
	
	if(this.lifeCurrent == undefined)
	{
		throw new TypeError('this.lifeCurrent is not defined');
	}
	if(value < 0)
	{
		throw new TypeError('value must be positive');
	}
	
	if(this.lifeCurrent <= value)
	{
		this.lifeCurrent = 0;
	}
	else
	{
		this.lifeCurrent -= value;
	}
};


TowerDefenseMapAttack.prototype.getPosition = function()
{
	"use strict";
	return this.rectangle.position;
};

TowerDefenseMapAttack.prototype.setDirectionFactorX = function(value)
{
	"use strict";
	if(value == undefined)
	{
		throw new TypeError('value is not defined');
	}
	if(isNaN(value))
	{
		throw new TypeError('value is not a number (typeof returns '+ typeof(value)+ ')');
	}
	this.directionFactorX = value;
};

TowerDefenseMapAttack.prototype.setDirectionFactorY = function(value)
{
	"use strict";
	if(value == undefined)
	{
		throw new TypeError('value is not defined');
	}
	if(isNaN(value))
	{
		throw new TypeError('value is not a number (typeof returns '+ typeof(value)+ ')');
	}
	this.directionFactorY = value;
};

TowerDefenseMapAttack.prototype.setDistancePerMs = function(value)
{
	"use strict";
	if(value == undefined)
	{
		throw new TypeError('value is not defined');
	}
	if(isNaN(distancePerMs))
	{
		throw new TypeError('value is not a number (typeof returns '+ typeof(value)+ ')');
	}
	this.distancePerMs = value;
};

TowerDefenseMapAttack.prototype.getWidth = function()
{
	"use strict";
	return this.rectangle.width;
};

TowerDefenseMapAttack.prototype.getHeight = function()
{
	"use strict";
	return this.rectangle.height;
};

TowerDefenseMapAttack.prototype.setPower = function(value)
{
	"use strict";
	if(value == undefined)
	{
		throw new TypeError('value is not defined');
	}
	if(isNaN(value))
	{
		throw new TypeError('value is not a number (typeof returns '+ typeof(value)+ ')');
	}
	this.power = value;
};


TowerDefenseMapAttack.prototype.move = function(clock)
{
	"use strict";
	this.rectangle.position.x += this.distancePerMs * this.directionFactorX * clock.deltaTime;
	this.rectangle.position.y += this.distancePerMs * this.directionFactorY * clock.deltaTime;
};

TowerDefenseMapAttack.prototype.update = function(clock)
{
	"use strict";
	this.move(clock);
};
