/*
 * Copyright (C) 2015  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


if(typeof(Array.isArray) != 'function')
{
	Array.isArray = function(arg)
	{
		return Object.prototype.toString.call(arg) === '[object Array]';
	};
}

if(typeof(Array.prototype.forEach) != 'function')
{
	Array.prototype.forEach = function(callback, thisArg)
	{
		'use strict';
		for(var i=0; i < this.length; ++i)
		{
			callback.call(thisArg, this[i], i, this);
		}
	};
}

if(typeof(Array.prototype.includes) != 'function')
{
	Array.prototype.includes = function(value)
	{
		'use strict';
		return this.indexOf(value) >= 0;
	};
}

if(typeof(Array.prototype.indexOf) != 'function')
{
	Array.prototype.indexOf = function(searchElement, fromIndex)
	{
		'use strict';
		for(var i = fromIndex >= 0 ? fromIndex : 0; i < this.length; ++i)
		{
			if(this[i] === searchElement)
			{
				return i;
			}
		}
		return -1;
	};
}
