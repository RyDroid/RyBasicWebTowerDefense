/*
 * Copyright (C) 2015-2016  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 * @param {Position2D} position
 * @param {Number} width
 * @param {Number} height
 */
function Rectangle(position, width, height)
{
	if(!(position instanceof Position2D))
	{
		throw new TypeError("position must be an instance of Position2D");
	}
	this.position = position;
	
	this.setWidth (width);
	this.setHeight(height);
}

/**
 * @param {Number} width
 */
Rectangle.prototype.setWidth = function(width)
{
	if(isNaN(width))
	{
		throw new TypeError('width is not a number (typeof returns '+ typeof(width)+ ')');
	}
	if(width < 0)
	{
		throw new RangeError('width must be a positive number ('+ width +')');
	}
	this.width = width;
};

/**
 * @param {Number} height
 */
Rectangle.prototype.setHeight = function(height)
{
	if(isNaN(height))
	{
		throw new TypeError('height is not a number (typeof returns '+ typeof(height)+ ')');
	}
	if(height < 0)
	{
		throw new RangeError('height must be a positive number ('+ height +')');
	}
	this.height = height;
};

Rectangle.prototype.getCopy = function()
{
	"use strict";
	return new Rectangle(this.position.getCopy(), this.width, this.height);
};

Rectangle.prototype.isInCollisionWith = function(rectangle)
{
	"use strict";
	return !(
		this.position.y + this.height < rectangle.position.y ||
		this.position.y > rectangle.position.y + rectangle.height ||
		this.position.x > rectangle.position.x + rectangle.width ||
		this.position.x + this.width < rectangle.position.x
	);
};
