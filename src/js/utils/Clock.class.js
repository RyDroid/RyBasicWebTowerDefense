/**
 * For that file :
 * @licence LGPL 2.1+ (https://www.gnu.org/licenses/lgpl-2.1.html)
 * @author Olivier Nocent <olivier.nocent@univ-reims.fr>
 * @author Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 */

/**
 * Create an instance of Clock
 * @constructor
 * @attribute boolean isRunning
 * @attribute {float} time
 * @attribute {float} deltaTime
 * @attribute {int}   lastTimeStamp
 */
function Clock()
{
	"use strict";
	this.reset();
}

/**
 * Start the clock
 */
Clock.prototype.start = function()
{
	"use strict";
	this.isRunning = true;
	var timer = new Date();
	this.lastTimeStamp = timer.getTime();
}

/**
 * Stop the clock
 */
Clock.prototype.stop = function()
{
	"use strict";
	this.isRunning = false;
	this.deltaTime = 0;
}

/**
 * Reset the clock
 */
Clock.prototype.reset = function()
{
	"use strict";
	this.stop();
	this.time = 0;
}

/**
 * Restart the clock
 */
Clock.prototype.restart = function()
{
	"use strict";
	this.reset();
	this.start();
}

Clock.prototype.update = function()
{
	"use strict";
	
	if (this.isRunning)
	{
		var timer = new Date();
		var currentTimeStamp = timer.getTime();
		this.deltaTime = currentTimeStamp - this.lastTimeStamp;
		
		this.lastTimeStamp = currentTimeStamp;
		
		this.time += this.deltaTime;
	}
}

/**
 * @return {float} time (in milliseconds) elapsed since the clock starts
 */
Clock.prototype.getTime = function()
{
	"use strict";
	return this.time;
}

/**
 * @return {float} time (in milliseconds) elapsed since the last call of update()
 */
Clock.prototype.getDeltaTime = function()
{
	"use strict";
	return this.deltaTime;
}
