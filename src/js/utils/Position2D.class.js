/*
 * Copyright (C) 2015, 2017  Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 * @param {int} x position
 * @param {int} y position
 */
function Position2D(x, y)
{
	"use strict";
	this.setX(x);
	this.setY(y);
}

/**
 * @param {int} x position
 */
Position2D.prototype.setX = function(x)
{
	if(isNaN(x))
	{
		throw new TypeError('x is not a number (typeof returns '+ typeof(x)+ ')');
	}
	this.x = x;
};

/**
 * @param {int} y position
 */
Position2D.prototype.setY = function(y)
{
	if(isNaN(y))
	{
		throw new TypeError('y is not a number (typeof returns '+ typeof(y)+ ')');
	}
	this.y = y;
};

Position2D.prototype.equalsUnsafe = function(position)
{
	"use strict";
	return this.x == position.x && this.y == position.y;
};

Position2D.prototype.equals = function(position)
{
	"use strict";
	return position instanceof Position2D && this.equalsUnsafe(position);
};

Position2D.prototype.getCopy = function()
{
	"use strict";
	return new Position2D(this.x, this.y);
};
