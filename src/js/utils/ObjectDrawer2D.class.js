/*
 * Copyright (C) 2015  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var ObjectDrawer2D = {};

ObjectDrawer2D.VERY_LOW_QUALITY_MODE  =  1;
ObjectDrawer2D.LOW_QUALITY_MODE       =  5;
ObjectDrawer2D.MEDIUM_QUALITY_MODE    = 10;
ObjectDrawer2D.HIGH_QUALITY_MODE      = 15;
ObjectDrawer2D.VERY_HIGH_QUALITY_MODE = 20;
ObjectDrawer2D.current_quality_mode   = ObjectDrawer2D.HIGH_QUALITY_MODE;

ObjectDrawer2D.drawBackgroundWithStyle = function(canvas, context, style)
{
	"use strict";
	context.fillStyle = style;
	context.fillRect(0, 0, canvas.width, canvas.height);
};

ObjectDrawer2D.drawImage = function(context, image, rectangle)
{
	"use strict";
	context.drawImage(
			image,
			rectangle.position.x,
			rectangle.position.y,
			rectangle.width,
			rectangle.height
		);
};
